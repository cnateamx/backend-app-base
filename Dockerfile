# Parent image used
FROM node:lts-alpine3.11
 
# Create app directory
WORKDIR /app
 
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./
 
RUN npm install
 
# Bundle app source
COPY . .
 
EXPOSE 8080
 
# Command to start application
CMD ["node", "server.js"]