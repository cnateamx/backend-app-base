const e = require('cors');
//var v=require('validator');
var validator = require('validator');

var validationFn={

    validateRegister:function(req,res,next){

       //Validation code to check register form input values
       //return response with status 500 if validation fails
       var username = req.body.username;
       var email = req.body.email;
       var password = req.body.password;

       var reUserName = new RegExp(`^[a-zA-Z ,']+$`)
       var rePassword = new RegExp(`^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}$`)

       var validUserName = reUserName.test(username)
       var validPassword = rePassword.test(password)
       var validEmail = validator.isEmail(email)

       if (!validUserName) console.log("Invalid username")
       if (!validPassword) console.log("Invalid password")
       if (!validEmail) console.log("Invalid email")

       if (validUserName && validPassword && validEmail){
           console.log("Passed test!")
           next();
       }
       else{
           res.type('json');
           console.log("Failed test!")
           res.status(500)
           res.send(`{"Message":"Data does not meet the input requirements!"}`)
       }
    },

    validateUserid: function (req, res, next) {
   //Validation code to check userid from req.params

       //return response with status 500 if validation fails

    },

    sanitizeResult:function(result){

        //Sanitize each record’s values from the database result        
        
    }


}

module.exports=validationFn;
